H/W path      Device      Class          Description
====================================================
                          system         Inspiron 7437 (Inspiron 7437)
/0                        bus            0XHV6M
/0/0                      processor      Intel(R) Core(TM) i5-4210U CPU @ 1.70GHz
/0/0/2                    memory         32KiB L1 cache
/0/0/3                    memory         256KiB L2 cache
/0/0/4                    memory         3MiB L3 cache
/0/1                      memory         32KiB L1 cache
/0/5                      memory         6GiB System Memory
/0/5/0                    memory         4GiB SODIMM DDR3 Synchronous 1600 MHz (0,6 ns)
/0/5/1                    memory         2GiB SODIMM DDR3 Synchronous 1600 MHz (0,6 ns)
/0/3e                     memory         128KiB BIOS
/0/2                      display        Haswell-ULT Integrated Graphics Controller
/0/3                      multimedia     Haswell-ULT HD Audio Controller
/0/14                     bus            8 Series USB xHCI HC
/0/14/0       usb2        bus            xHCI Host Controller
/0/14/0/1                 input          Dell Wireless Mouse WM123
/0/14/0/5                 multimedia     Integrated_Webcam_HD
/0/14/0/6                 communication  Bluetooth wireless interface
/0/14/0/7                 input          Touchscreen
/0/14/1       usb3        bus            xHCI Host Controller
/0/16                     communication  8 Series HECI #0
/0/1b                     multimedia     8 Series HD Audio Controller
/0/1c                     bridge         8 Series PCI Express Root Port 1
/0/1c.2                   bridge         8 Series PCI Express Root Port 3
/0/1c.2/0     wlp2s0      network        Wireless 7260
/0/1c.3                   bridge         8 Series PCI Express Root Port 4
/0/1c.3/0                 generic        RTS5227 PCI Express Card Reader
/0/1d                     bus            8 Series USB EHCI #1
/0/1d/1       usb1        bus            EHCI Host Controller
/0/1d/1/1                 bus            USB hub
/0/1f                     bridge         8 Series LPC Controller
/0/1f.2                   storage        8 Series SATA Controller 1 [AHCI mode]
/0/1f.3                   bus            8 Series SMBus Controller
/1                        power          DELL C4MF837
/2            virbr0-nic  network        Ethernet interface
/3            virbr0      network        Ethernet interface
